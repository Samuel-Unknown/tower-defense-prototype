﻿using UnityEngine;

using TDGame;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TDGameEditor {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(WeaponGun))]
    public class WeaponGunEditor : Editor {
        private WeaponGun weaponGunTarget;

        private SerializedProperty weaponProperty;
        private SerializedProperty projectilePrefab;    
        private SerializedProperty pooledAmount;                    

        private Editor weaponPropertyEditor;
        private MonoScript script;

        void OnEnable() {
            weaponGunTarget = (WeaponGun)target;

            script = MonoScript.FromMonoBehaviour(weaponGunTarget);

            weaponProperty = serializedObject.FindProperty("weaponProperty");
            projectilePrefab = serializedObject.FindProperty("projectilePrefab");
            pooledAmount = serializedObject.FindProperty("pooledAmount");
        
            ResetWeaponGunPropertyEditor();
        }

        /// <summary>
        /// Сброс Editor'а weaponProperty
        /// </summary>
        private void ResetWeaponGunPropertyEditor() {
            weaponPropertyEditor = Editor.CreateEditor(weaponProperty.objectReferenceValue); 
        }

        public override void OnInspectorGUI() {
            
            serializedObject.Update();   
            
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false);        
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginChangeCheck(); 
            EditorGUILayout.PropertyField(weaponProperty);
            if (EditorGUI.EndChangeCheck()) { 
                serializedObject.ApplyModifiedProperties(); // поскольку изменили значение PropertyField(weaponProperty) 
                                                            // нужно применить изменения и только после этого
                                                            // перезагрузить Editor weaponProperty
                ResetWeaponGunPropertyEditor();
            } 
            
            EditorGUILayout.PropertyField(pooledAmount);            
            EditorGUILayout.PropertyField(projectilePrefab);

            if (projectilePrefab.objectReferenceValue == null) {
                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("Projectile Prefab не должен быть пустым!", MessageType.Error);
            }

            // Рисуем weaponPropertyEditor
            if (weaponPropertyEditor != null) {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUI.BeginDisabledGroup(true);
                weaponPropertyEditor.OnInspectorGUI(); 
                EditorGUI.EndDisabledGroup(); 
                EditorGUILayout.EndVertical();
            } else {
                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("WeaponGun Property не должен быть пустым!", MessageType.Error); 
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}