﻿using UnityEngine;

using TDGame;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TDGameEditor {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(Monster))]
    public class MonsterEditor : Editor {
        private Monster monsterTarget;

        private SerializedProperty monsterProperty;
        private Editor monsterPropertyEditor;
        private MonoScript script;

        void OnEnable() {
            monsterTarget = (Monster)target;

            script = MonoScript.FromMonoBehaviour(monsterTarget);

            monsterProperty = serializedObject.FindProperty("monsterProperty");
        
            ResetMonsterPropertyEditor();
        }

        /// <summary>
        /// Сброс Editor'а monsterProperty
        /// </summary>
        private void ResetMonsterPropertyEditor() {
            monsterPropertyEditor = Editor.CreateEditor(monsterProperty.objectReferenceValue); 
        }

        public override void OnInspectorGUI() {
            
            serializedObject.Update();   
            
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false);        
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginChangeCheck(); 
            EditorGUILayout.PropertyField(monsterProperty);
            if (EditorGUI.EndChangeCheck()) { 
                serializedObject.ApplyModifiedProperties(); // поскольку изменили значение PropertyField(monsterProperty) 
                                                            // нужно применить изменения и только после этого
                                                            // перезагрузить Editor monsterProperty
                ResetMonsterPropertyEditor();
            } 
            
            // Рисуем monsterPropertyEditor
            if (monsterPropertyEditor != null) {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUI.BeginDisabledGroup(true);
                monsterPropertyEditor.OnInspectorGUI(); 
                EditorGUI.EndDisabledGroup(); 
                EditorGUILayout.EndVertical();
            } else {
                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("Monster Property не должен быть пустым!", MessageType.Error); 
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}

