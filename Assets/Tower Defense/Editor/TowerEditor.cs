﻿using UnityEngine;

using TDGame;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TDGameEditor {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(Tower))]
    public class TowerEditor : Editor {
        private Tower towerTarget;

        private SerializedProperty towerProperty;
        private Editor towerPropertyEditor;
        private MonoScript script;

        void OnEnable() {
            towerTarget = (Tower)target;

            script = MonoScript.FromMonoBehaviour(towerTarget);

            towerProperty = serializedObject.FindProperty("towerProperty");
        
            ResetTowerPropertyEditor();
        }

        /// <summary>
        /// Сброс Editor'а towerProperty
        /// </summary>
        private void ResetTowerPropertyEditor() {
            towerPropertyEditor = Editor.CreateEditor(towerProperty.objectReferenceValue); 
        }

        public override void OnInspectorGUI() {
            
            serializedObject.Update();   
            
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false);        
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginChangeCheck(); 
            EditorGUILayout.PropertyField(towerProperty);
            if (EditorGUI.EndChangeCheck()) { 
                serializedObject.ApplyModifiedProperties(); // поскольку изменили значение PropertyField(towerProperty) 
                                                            // нужно применить изменения и только после этого
                                                            // перезагрузить Editor towerProperty
                ResetTowerPropertyEditor();
            } 
            
            // Рисуем towerPropertyEditor
            if (towerPropertyEditor != null) {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUI.BeginDisabledGroup(true);
                towerPropertyEditor.OnInspectorGUI(); 
                EditorGUI.EndDisabledGroup(); 
                EditorGUILayout.EndVertical();
            } else {
                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("Tower Property не должен быть пустым!", MessageType.Error); 
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}