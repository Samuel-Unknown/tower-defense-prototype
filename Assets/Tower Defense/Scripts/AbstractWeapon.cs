﻿using UnityEngine;
using System.Collections;

namespace TDGame {

	/// <summary>
    /// Абстрактный класс для оружия
    /// </summary>
	public abstract class AbstractWeapon : MonoBehaviour {

		/// <summary>
        /// Экземпляр класса WeaponProperty характеризующего оружие башни
        /// </summary>
		[SerializeField, Tooltip("Экземпляр класса WeaponProperty характеризующего оружие башни")]
		protected WeaponProperty weaponProperty;

		/// <summary>
        /// Есть ли у оружия текущая цель
        /// </summary>
		private bool haveTarget = false;

		/// <summary>
        /// Глобальные координаты текущей цели
        /// </summary>
		private Vector3 currentTarget;

		/// <summary>
        /// Устанавливает текущую цель
        /// </summary>
        /// <param name="haveTarget">true - если текущая цель имеется, false - если нет</param>
        /// <param name="target">Глобальные координаты текущей цели</param>
		public void SetTarget(bool haveTarget, Vector3 target) {
			this.haveTarget = haveTarget;

			if (haveTarget)
				currentTarget = target;
			else 
				currentTarget = Vector3.zero;
		}

		void Start () {
			StartCoroutine( Fire(weaponProperty.RateOfFire) );
		}

		/// <summary>
        /// Корутин для ведения огня
        /// </summary>
        /// <param name="count">Частота с которой должны производится выстрелы [сек]</param>
		private IEnumerator Fire(float count) {
				
			while (true) {            	
				if (haveTarget)
					Shoot(currentTarget);
				
				yield return new WaitForSeconds(count);
        	}
		}

		/// <summary>
        /// Осуществляет выстрел по заданной цели
        /// </summary>
        /// <param name="target">Глобальные координаты цели</param>
		protected abstract void Shoot(Vector3 target);
		// в одной из реализаций например можно пускать рейкас для изображения 
		// лазерного оружия и отнимания жизней у монстров, в другой здесь могут 
		// создаваться ракеты которые будут лететь от башни до монстра
	}
}

