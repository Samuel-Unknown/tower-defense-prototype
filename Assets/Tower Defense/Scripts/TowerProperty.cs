﻿using UnityEngine;

namespace TDGame {

	/// <summary>
    /// Класс характеризующий башню 
    /// </summary>
	[CreateAssetMenu(menuName = "TDGame/Tower Property")]
	public class TowerProperty : ScriptableObject {

		/// <summary>
        /// Радиус обнаружения целей [м]
        /// </summary>
		[SerializeField, Range(0.1f, 10), Tooltip("Радиус обнаружения целей [м]")]
		private float detectionRadius = 1;

		/// <summary>
        /// Возвращает радиус обнаружения целей [м]
        /// </summary>
		public float DetectionRadius {
			get { return detectionRadius; }
		}

		/// <summary>
        /// Начальный уровень здоровья
        /// </summary>
		[SerializeField, Range(1, 250), Tooltip("Начальный уровень здоровья")]
		private float health = 50;

		/// <summary>
        /// Возвращает начальный уровень здоровья
        /// </summary>
		public float Health {
			get { return health; }
		}

		/// <summary>
        /// Стоимость башни в магазине
        /// </summary>
		[SerializeField, Range(0, 5000), Tooltip("Стоимость башни в магазине")]
		private int cost = 50;

		/// <summary>
        /// Возвращает стоимость башни в магазине
        /// </summary>
		public int Cost { 
			get { return cost; }
		}
	}
}
