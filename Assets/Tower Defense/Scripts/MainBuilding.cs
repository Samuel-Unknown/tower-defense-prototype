﻿using UnityEngine;
using System;


namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания события изменения текущего уровня жизней главного здания
    /// </summary>
	public interface IMainBuildingHealthChanged {
		/// <summary>
        /// Callback метод вызываемый при изменение текущего уровня жизней главного здания
        /// </summary>
		void MainBuildingHealthChanged(float currentHealth);
	}

	/// <summary>
    /// Интерфейс для отлавливания события проигрыша в игре
    /// </summary>
	public interface IGameower {

		/// <summary>
        /// Callback метод вызываемый при гибели гавного здания
        /// </summary>
		void Gameower();
	}

	/// <summary>
    /// Компонент главного здания к которому идут монстры 
    /// </summary>
	[SelectionBase, DisallowMultipleComponent, RequireComponent (typeof (Rigidbody)), RequireComponent (typeof (BoxCollider))]
	public class MainBuilding : MonoBehaviour, IStartGame {

		/// <summary>
        /// Начальный уровень здоровья
        /// </summary>
		[SerializeField, Range(1, 250), Tooltip("Начальный уровень здоровья")]
		private float health = 50;

		/// <summary>
        /// Текущий уровень здоровья
        /// </summary>
		private float currentHealth;

		/// <summary>
        /// Возвращает текущий уроень здровья
        /// </summary>
		public float CurrentHealth {
			get { return currentHealth; }
		}
		
		/// <summary>
        /// Маска для нахождения врага 
        /// </summary>
		private int enemylayerMask;

		/// <summary>
        /// Callback Event для события гибели гавного здания
        /// </summary>
		private event Action gameowerCallbackEvent;

		/// <summary>
        /// Добавляет callback для события гибели гавного здания
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(IGameower i) {
            gameowerCallbackEvent += i.Gameower;
        }

		/// <summary>
        /// Открепляет callback для события гибели гавного здания
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(IGameower i) {
            gameowerCallbackEvent -= i.Gameower;
        }

		/// <summary>
        /// Callback Event для события изменения текущего уровня жизней главного здания
        /// </summary>
		private event Action<float> mainBuildingHealthChangedCallbackEvent;

		/// <summary>
        /// Добавляет callback для события изменения текущего уровня жизней главного здания
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(IMainBuildingHealthChanged i) {
            mainBuildingHealthChangedCallbackEvent += i.MainBuildingHealthChanged;
        }

		/// <summary>
        /// Открепляет callback для события изменения текущего уровня жизней главного здания
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(IMainBuildingHealthChanged i) {
            mainBuildingHealthChangedCallbackEvent -= i.MainBuildingHealthChanged;
        }

		void Awake() {
			GetComponent<Rigidbody>().isKinematic = true;
			GetComponent<BoxCollider>().isTrigger = true;
			
			enemylayerMask = LayerMask.GetMask("Enemy");

			GUILogic guiLogic = GameObject.FindObjectOfType<GUILogic>();
			if (guiLogic)
				guiLogic.AddCallback(this);
		}

		/// <summary>
        /// Callback метод вызываемый при запуске игры
        /// </summary>
		public void StartGame() {
			gameObject.SetActive(true);
			currentHealth = health; 
	
			if (mainBuildingHealthChangedCallbackEvent != null)
				mainBuildingHealthChangedCallbackEvent(currentHealth);
		}

		void OnEnable() {
			StartGame();
		} 

		void OnDisable() {
			gameowerCallbackEvent = null;
		}

		/// <summary>
        /// Наносит повреждения главному зданию
        /// </summary>
        /// <param name="damage">Урон</param>		
		public virtual void TakeDamage(float damage) {
			currentHealth = Mathf.Clamp(currentHealth - damage, 0, health);

			if (mainBuildingHealthChangedCallbackEvent != null)
				mainBuildingHealthChangedCallbackEvent(currentHealth);

			if (currentHealth == 0) {
				if (gameowerCallbackEvent != null)
	  		      	gameowerCallbackEvent();

				gameObject.SetActive(false);				
			}
		}

		void OnTriggerEnter(Collider other) {
			int layerMask = 1 << other.gameObject.layer;

			// если контакт произошёл с монстром
			if ( (layerMask & enemylayerMask) != 0) {
				Monster monster = other.GetComponent<Monster>();
				// наносим урон главному зданию
				TakeDamage(monster.ContactDamage);
				
				// убиваем монстра
				monster.Die();
			}
		}
	}
}
