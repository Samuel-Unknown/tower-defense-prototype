﻿using UnityEngine;
using System;

namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания событий строительства или отмены строительства башни
    /// </summary>
	public interface ICancelOrBuildTower {

		/// <summary>
        /// Callback метод вызываемый при отмене строительства башни
        /// </summary>
		void CanceledBuildTower();

		/// <summary>
        /// Callback метод вызываемый при удачном строительстве башни
        /// </summary>
		void BuildedTower();
	}

	/// <summary>
    /// Компонент для строительства башни, навешивается на башню-призрак 
    /// </summary>
	[SelectionBase, DisallowMultipleComponent, RequireComponent (typeof (BoxCollider))]
	public class BuildTower : MonoBehaviour {

		/// <summary>
		/// Главная камера 
		/// </summary>
		private Camera mainCamera;

		/// <summary>
		/// Маска для нахождения Building Area
		/// </summary>
		private int buildingArealayerMask;

		/// <summary>
		/// Меш башни-призрака для отображения когда строительство разрешено
		/// </summary>
		[SerializeField, Tooltip("Меш башни-призрака для отображения когда строительство разрешено")]
		private GameObject greenGhost;

		/// <summary>
		/// Меш башни-призрака для отображения когда строительство запрещено
		/// </summary>
		[SerializeField, Tooltip("Меш башни-призрака для отображения когда строительство запрещено")]
		private GameObject redGhost;

		/// <summary>
        /// Префаб башни которая будет построена
        /// </summary>
		[SerializeField, Tooltip("Префаб башни которая будет построена")]
		private GameObject towerPref;

		/// <summary>
		/// Меш для отображения зоны в области которой будут обнаруживаться монстры
		/// </summary>
		[SerializeField, Tooltip("Меш для отображения зоны в области которой будут обнаруживаться монстры")]
		private GameObject detectionZone;

		/// <summary>
		/// Разрешено ли в данной области строительство башни  
		/// </summary>
		private bool allow;

		/// <summary>
		/// Коллизит ли башня с чем-то из-за чего нельзя строить 
		/// </summary>
		private bool collision;

		/// <summary>
		/// Находится ли башня на поверхности для строительства
		/// </summary>
		private bool buildingArea;

		void Awake() {
			GetComponent<BoxCollider>().isTrigger = true;
			
			buildingArealayerMask = LayerMask.GetMask("Building Area");		
			mainCamera = Camera.main;

			Tower tower = towerPref.GetComponent<Tower>();
			if (detectionZone != null && tower != null)
				detectionZone.transform.localScale = new Vector3(tower.DetectionRadius * 2, 0.01f ,tower.DetectionRadius * 2);
		}

		void Update () {
			UpdatePosition();
			UpdateState();

			if (Input.GetMouseButtonDown(0))
            	Build();
        
        	if (Input.GetMouseButtonDown(1))
            	Cancel();
		}

		void OnTriggerStay(Collider other) {
			collision = true;
		}

		void OnTriggerExit(Collider other) {
			collision = false;
		}

		/// <summary>
		/// Обновляет состояние отвечающее за разрешение строительства
		/// </summary>
		private void UpdateState() {
			
			allow = buildingArea && !collision;

			if (greenGhost != null)
				greenGhost.SetActive(allow);
			if (redGhost != null)
				redGhost.SetActive(!allow);	
		} 

		/// <summary>
		/// Обновлет положение башни-призрака в пространстве 
		/// </summary>
		private void UpdatePosition() {
			
			// Положение башни в пространстве  
			Vector3 objPosition;

			Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, mainCamera.farClipPlane, buildingArealayerMask)) {
				buildingArea = true;
				objPosition = hit.point;
			} else {
				buildingArea = false;		
				Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 50); // компонента z отвечает за то, на каком расстоянии находится объект от ближней плоскости отсечения камеры
				objPosition = mainCamera.ScreenToWorldPoint(mousePosition);
			}

			transform.position = objPosition;
		}

		/// <summary>
        /// Строительство башни 
        /// </summary>
		private void Build() {
			if (!allow || towerPref == null)
				return;

			Tower tower = towerPref.GetComponent<Tower>();
			if (tower == null)
				return;

			GameInfo.Instance.Coins -= tower.Cost;

			if (buildTowerCallbackEvent != null)
				buildTowerCallbackEvent();

			Instantiate(towerPref, transform.position, transform.rotation);
			Destroy(gameObject);
		}

		/// <summary>
        /// Отмена строительства
        /// </summary>
		private void Cancel() {
			if (cancelBuildTowerCallbackEvent != null)
				cancelBuildTowerCallbackEvent();
				
			Destroy(gameObject);
		}

		/// <summary>
        /// Callback Event для события отмены строительства башни
        /// </summary>
		private event Action cancelBuildTowerCallbackEvent;

		/// <summary>
        /// Добавляет callback для события отмены строительства башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCancleBuildTowerCallback(ICancelOrBuildTower i) {
            cancelBuildTowerCallbackEvent += i.CanceledBuildTower;
        }

		/// <summary>
        /// Открепляет callback для события отмены строительства башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCancleBuildTowerCallback(ICancelOrBuildTower i) {
            cancelBuildTowerCallbackEvent -= i.CanceledBuildTower;
        }

		/// <summary>
        /// Callback Event для строительства башни
        /// </summary>
		private event Action buildTowerCallbackEvent;

		/// <summary>
        /// Добавляет callback для строительства башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddBuildTowerCallback(ICancelOrBuildTower i) {
            buildTowerCallbackEvent += i.BuildedTower;
        }

		/// <summary>
        /// Открепляет callback для строительства башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveBuildTowerCallback(ICancelOrBuildTower i) {
            buildTowerCallbackEvent -= i.BuildedTower;
        }

	}
}
