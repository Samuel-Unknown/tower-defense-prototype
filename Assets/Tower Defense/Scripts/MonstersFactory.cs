﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания события окончания всех волн
    /// </summary>
	public interface IAllWavesEnded {

		/// <summary>
        /// Callback метод вызываемый при завершении всех волн
        /// </summary>
		void AllWavesEnded();
	}

	/// <summary>
    /// Интерфейс для отлавливания события окончания конкретной волны
    /// </summary>
	public interface IWaveEnded {
		/// <summary>
        /// Callback метод вызываемый при завершении очередной волны 
        /// </summary>
		void WaveEnded();
	}

	/// <summary>
    /// Интерфейс для получения информации о номере текущей волны и их общем количестве 
    /// </summary>
	public interface IWavesInfo {
		/// <summary>
        /// Callback метод для получения информации о номере текущей волны и их общем количестве 
        /// </summary>
        /// <param name="currentWave">номер текущей волны</param>
        /// <param name="maxWave">максимальное число волн</param>
		void WavesInfo(int currentWave, int maxWave);
	}


	/// <summary>
    /// Компонент фабрики монстров
    /// </summary>
	[SelectionBase, DisallowMultipleComponent]
	public class MonstersFactory : MonoBehaviour, IWaveEnded, IStartGame, IGameower {
		
		/// <summary>
        /// Список всех волн
        /// </summary>
		[SerializeField, Tooltip("Список всех волн")]
		private List<Wave> waves = new List<Wave>();

		/// <summary>
        /// Возвращает общее количество волн
        /// </summary>
		public int WavesCount {
			get { return waves.Count; }
		}

		/// <summary>
        /// Закончилась ли последняя из начатых волн  
        /// </summary>
		private bool waveEnded;

		/// <summary>
        /// Callback метод вызываемый при завершении очередной волны 
        /// </summary>
		public void WaveEnded() {
			// Debug.Log("Поймали событие о конце волны # " + currentWave);
			currentWave++;
			waveEnded = true;

			wavesInfoCallbackEvent(CurrentWave, waves.Count);
		}

		/// <summary>
        /// Callback Event для события конца всех волн
        /// </summary>
		private event Action allWavesEndedCallbackEvent;

		/// <summary>
        /// Добавляет callback для события конца всех волн
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallbackWavesEnded(IAllWavesEnded i) {
            allWavesEndedCallbackEvent += i.AllWavesEnded;
        }

		/// <summary>
        /// Открепляет callback для события конца всех волн
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallbackWavesEnded(IAllWavesEnded i) {
            allWavesEndedCallbackEvent -= i.AllWavesEnded;
        }

		/// <summary>
        /// Callback Event для события конца всех волн
        /// </summary>
		private event Action<int, int> wavesInfoCallbackEvent;

		/// <summary>
        /// Добавляет callback для события конца всех волн
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallbackWavesInfo(IWavesInfo i) {
            wavesInfoCallbackEvent += i.WavesInfo;
        }

		/// <summary>
        /// Открепляет callback для события конца всех волн
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallbackWavesInfo(IWavesInfo i) {
            wavesInfoCallbackEvent -= i.WavesInfo;
        }

		void Awake() {
			// Инициализируем пулы всех волн
			for (int i = 0; i < waves.Count; i++) {
				waves[i].InitPool();
				waves[i].AddCallback(this);
			}

			GUILogic guiLogic = GameObject.FindObjectOfType<GUILogic>();
			if (guiLogic)
				guiLogic.AddCallback(this);
		}

		Coroutine wavesGenerationLastRoutine = null;
		Coroutine monstersGenerationLastRoutine = null;

		/// <summary>
        /// Callback метод вызываемый при запуске игры
        /// </summary>
		public void StartGame() {
			MainBuilding mainBuilding = GameObject.FindObjectOfType<MainBuilding>();
			if (mainBuilding)
				mainBuilding.AddCallback(this);

			wavesGenerationLastRoutine = StartCoroutine( WavesGeneration() );	
		}

		/// <summary>
        /// Callback метод вызываемый при гибели гавного здания
        /// </summary>
		public void Gameower() {
			StopWave();

			StopCoroutine( wavesGenerationLastRoutine );	

			DeactivateRemainingWaves();
		}

		/// <summary>
        /// Текущий номер волны (начинается с 1) 
        /// </summary>
		private int currentWave;

		/// <summary>
        /// Возвращает текущий номер волны (начинается с 1) 
        /// </summary>
		public int CurrentWave {
			// тут специально сделано так что при обращении через CurrentWave
			// нельзя было получить номер текущей волны больший чем максимальное число волн,
			// потому как currentWave может стать больше на 1 максимального числа волн, 
			// когда заканчивается последняя волна

			get { return Mathf.Clamp(currentWave, 1, waves.Count); }
		}
		
		/// <summary>
        /// Запуск волны согласно текущему номеру currentWave
        /// </summary>
		private void StartWave() {
			monstersGenerationLastRoutine = StartCoroutine( waves[currentWave - 1].MonstersGeneration() );
			waveEnded = false;
		}

		/// <summary>
        /// Экстренная отсановка последней запущенной волны
        /// </summary>
		private void StopWave() {
			StopCoroutine( monstersGenerationLastRoutine );
			waveEnded = true;
		}

		/// <summary>
        /// Деактивирует пулы монстров всех волн
        /// </summary>
		private void DeactivateRemainingWaves() {
			for (int i = 0; i < waves.Count; i++) {
				waves[i].DeactivatePool();
			}
		}

		/// <summary>
        /// Корутин для генерации волн
        /// </summary>
		private IEnumerator WavesGeneration() {
			currentWave = 1;
			wavesInfoCallbackEvent(1, waves.Count);
			StartWave();
			
			while (currentWave <= waves.Count) {
				
				if (waveEnded) {
					yield return new WaitForSeconds(waves[currentWave - 1].NextWaveDelay);
					// Debug.Log("Запуск новой волны");
					StartWave();
				}
				
				yield return null;					
			}

			// Волны закончились, а значит можно вызвать callback для информирования об этом всех подписавшихся 
			if (allWavesEndedCallbackEvent != null)
				allWavesEndedCallbackEvent();

			// Debug.Log("* Волны закончились *");
		}
	}

	/// <summary>
    /// Класс одной волны монстров 
    /// </summary>
	[System.Serializable]
	public class Wave : IMonsterDied {

		public Wave() {
			monsters = new List<GameObject>();
		}

		/// <summary>
        /// Инициализирует пул волны
        /// </summary>
		public void InitPool() {
			for (int i = 0; i < squads.Count; i++) {
				int c = squads[i].MonstersCount;

				for (int j = 0; j < c; j++) {
					GameObject monsterGO = UnityEngine.Object.Instantiate(squads[i].MonsterGO, Vector3.zero, Quaternion.identity);
					monsterGO.SetActive(false);

					monsters.Add(monsterGO);
				}
			}
		}

		/// <summary>
        /// Делает всех монстров из пула неактивными 
        /// </summary>
		public void DeactivatePool() {
			for (int i = 0; i < monsters.Count; i++) {
				monsters[i].SetActive(false);
			}
		}

		/// <summary>
        /// Callback метод вызываемый после смерти монстра 
        /// </summary>
		public void MonsterDied() {
			deadMonsters++;

			// если уничтожен последний монстр из всего пула
			// говорим что волна закончилась
			if (deadMonsters == monsters.Count)
				End();
		}

		/// <summary>
        /// Интекс текущего монстра из пула
        /// </summary>
		private int currentMonsterIndex = 0;

		/// <summary>
        /// Количество уничтоженных монстров из этой волны
        /// </summary>
		private int deadMonsters = 0;

		/// <summary>
        /// Выпускает очередного монстра 
        /// </summary>
		private void ReleaseMonster() {
			monsters[currentMonsterIndex].transform.position = GetGatePosition();
			monsters[currentMonsterIndex].SetActive(true);
			monsters[currentMonsterIndex].GetComponent<Monster>().AddCallback(this);

			currentMonsterIndex++;
		}

		/// <summary>
        /// Корутин для генерации монстров
        /// </summary>
		public IEnumerator MonstersGeneration() {
			currentMonsterIndex = 0;
			deadMonsters = 0;
			
			while (currentMonsterIndex < monsters.Count) {
				
				ReleaseMonster();

				yield return new WaitForSeconds(generationRate);
			}
		}

		/// <summary>
        /// Callback Event для события конца волны
        /// </summary>
		private event Action endedCallbackEvent;

		/// <summary>
        /// Добавляет callback для события конца волны
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(IWaveEnded i) {
            endedCallbackEvent += i.WaveEnded;
        }

		/// <summary>
        /// Открепляет callback для события конца волны
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(IWaveEnded i) {
            endedCallbackEvent -= i.WaveEnded;
        }

		/// <summary>
        /// Завершить волну 
        /// </summary>
		private void End() {
			if (endedCallbackEvent != null)
	        	endedCallbackEvent();
		}

		/// <summary>
     	/// Частота генерации монстров [сек]
        /// </summary>
		[SerializeField, Range(0.1f, 5), Tooltip("Частота генерации монстров [сек]")]
		private float generationRate = 0.5f;

		/// <summary>
        /// Задержка с которой появится следующая волна [сек] 
        /// </summary>
		[SerializeField, Range(1, 10), Tooltip("Задержка с которой появится следующая волна [сек]")]
		private float nextWaveDelay = 2;

		/// <summary>
        /// Возвращает задержку с которой появится следующая волна [сек] 
        /// </summary>
		public float NextWaveDelay {
			get { return nextWaveDelay; }
		}

		/// <summary>
        ///	Список из команд монстров
        /// </summary>
		[SerializeField, Tooltip("Список из команд монстров")]
		private List<MonstersSquad> squads = new List<MonstersSquad>();

		/// <summary>
        ///	Список врат из которых будут появляться монстры
        /// </summary>
		[SerializeField, Tooltip("Список врат из которых будут появляться монстры")]
		private List<Transform> gates = new List<Transform>();

		/// <summary>
        /// Возвращает положение одного из доступных врат (выбор делается случайным образом) 
        /// </summary>
        /// <returns></returns>
		private Vector3 GetGatePosition() {
			if (gates.Count == 0)
				return Vector3.zero;

			int r = UnityEngine.Random.Range(0, gates.Count);  // возвращает int в диапозоне от 0 до Count - 1 включительно
			Transform tr = gates[r];

			if (tr != null)
				return tr.position;

			return Vector3.zero;
		}

		/// <summary>
        /// Пул всех монстров этой волны
        /// </summary>
		private List<GameObject> monsters;

	}

	/// <summary>
    /// Структура описывающая команду монстров 
    /// </summary>
	[System.Serializable]
	public struct MonstersSquad {

		/// <summary>
        /// Количество монстров в команде 
        /// </summary>
		[SerializeField, Tooltip("Количество монстров в команде")]
		private int monstersCount;

		/// <summary>
        /// Возвращает количество монстров в команде
        /// </summary>
		public int MonstersCount { 
			get { return monstersCount; } 
		}

		/// <summary>
        /// Префаб монстра котрый будет основой всех монстров в команде 
        /// </summary>
		[SerializeField, Tooltip("Префаб монстра котрый будет основой всех монстров в команде ")]		
		private GameObject monsterGO;

		/// <summary>
        /// Возвращает префаб монстра котрый будет основой всех монстров в команде 
        /// </summary>
		public GameObject MonsterGO {
			get { return monsterGO; }
		}

	}
}
