﻿using UnityEngine;

namespace TDGame {

	/// <summary>
    /// Компонент снаряда для оружия башни
    /// </summary>
	[SelectionBase, DisallowMultipleComponent, RequireComponent (typeof (Rigidbody)), RequireComponent (typeof (SphereCollider))]
	public class Projectile : MonoBehaviour {

		/// <summary>
		/// Урон наносимый снарядом
		/// </summary>
		public float Damage { set; get; }

		/// <summary>
        /// Скорость снаряда 
        /// </summary>
		[SerializeField, Range(1, 250), Tooltip("Скорость снаряда")]
		private float speed = 30;

		/// <summary>
        /// Вектор направленный в сторону цели 
        /// </summary>
		public Vector3 TargetDirection { set; get; }	

		/// <summary>
        /// Rigidbody снаряда
        /// </summary>
		private Rigidbody rb;

		/// <summary>
        /// Маска для нахождения врага 
        /// </summary>
		private int enemylayerMask;

		/// <summary>
        /// Маска для нахождения Play Zone Boundary  
        /// </summary>
		private int boundarylayerMask;

		void Awake() {
			GetComponent<SphereCollider>().isTrigger = true;
			rb = GetComponent<Rigidbody>();
			rb.isKinematic = false;

			enemylayerMask = LayerMask.GetMask("Enemy");
			boundarylayerMask = LayerMask.GetMask("Boundary");
		}

		void OnDisable() {
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		}
		
		void FixedUpdate() {
			rb.AddForce( TargetDirection.normalized * speed, ForceMode.Acceleration );
		}

		/// <summary>
        /// Наносим урон монстру
        /// </summary>
        /// <param name="m">Компонент Monster</param>
		private void TakeDamage(Monster m) {
			if (m != null)
				m.TakeDamage(Damage);
		}

		/// <summary>
		/// Самоуничтожение снаряда 
		/// </summary>
		public void SelfDestruction() {
			gameObject.SetActive(false);
		}

		void OnTriggerEnter(Collider other) {
			int layerMask = 1 << other.gameObject.layer;

			// не реагируем на то что снаряд внутри Play Zone Boundary
			if ( (layerMask & boundarylayerMask) != 0)
				return;

			// если попали в монстра
			if ( (layerMask & enemylayerMask) != 0) {
				TakeDamage(other.GetComponent<Monster>());
			}
			
			SelfDestruction();
		}
	}
}