﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using System;
using System.Collections.Generic;

namespace TDGame {

	/// <summary>
    /// Интерфейс для для отлавливани события создания или отмены создания башни
    /// </summary>
	public interface ISetNewTower {

		/// <summary>
        /// Callback метод вызываемый для события создания или отмены создания башни
        /// </summary>
		void SetNewTower();
	}

	/// <summary>
    /// Компонент для логики работы магазина башен (вкл/выкл UI кнопки)
    /// </summary>
	[DisallowMultipleComponent]
	public class TowersShop : MonoBehaviour, ISetNewTower, IGameower, IAllWavesEnded {

		/// <summary>
        /// Список всех башен для продажи 
        /// </summary>
		[SerializeField]
		private List<ShopTowerItem> towers = new List<ShopTowerItem>();

		void Awake() {
			
			MainBuilding mainBuilding = GameObject.FindObjectOfType<MainBuilding>();
			if (mainBuilding)
				mainBuilding.AddCallback((IGameower)this);		

			MonstersFactory monstersFactory = GameObject.FindObjectOfType<MonstersFactory>();
			if (monstersFactory)
				monstersFactory.AddCallbackWavesEnded((IAllWavesEnded)this);	


			for (int i = 0; i < towers.Count; i++) {
				towers[i].SetCost();
				towers[i].UpdateButtonsInteractable(GameInfo.Instance.Coins);
				towers[i].SetButtonToListenCreateGhostTower();
				towers[i].AddButtonListener(DisableButtons);
				towers[i].AddCallback(this);
			}
		}


		/// <summary>
        /// Обновляет интерактивность кнопок выбора башен согласно имеющимся в распоряжении монетам 
        /// </summary>
		private void UpdateButtonsInteractable() {
			for (int i = 0; i < towers.Count; i++) 				
				towers[i].UpdateButtonsInteractable(GameInfo.Instance.Coins);
		}

		/// <summary>
        /// Callback метод вызываемый при 
        /// </summary>
		public void SetNewTower() {
			UpdateButtonsInteractable();
		}

		/// <summary>
        /// Callback метод вызываемый при гибели гавного здания
        /// </summary>
		public void Gameower() {
			UpdateButtonsInteractable();
		}

		/// <summary>
        /// Callback метод вызываемый при завершении всех волн
        /// </summary>
		public void AllWavesEnded() {
			UpdateButtonsInteractable();
		}

		/// <summary>
        /// Делает все кнопки для покупок башен неактивными
        /// </summary>
		private void DisableButtons() {
			for (int i = 0; i < towers.Count; i++)
				towers[i].DisableButton();
		}
	}

	/// <summary>
    /// Класс описывающий одну башню в магазине
    /// </summary>
	[System.Serializable]
	public class ShopTowerItem : ICancelOrBuildTower {

		/// <summary>
        /// Префаб башни-призрака
        /// </summary>
		[SerializeField, Tooltip("Префаб башни-призрака")]
		private GameObject towerGhostPref;

		/// <summary>
        /// Экземпляр класса TowerProperty характеризующего башню
        /// </summary>
		[SerializeField, Tooltip("Экземпляр класса TowerProperty характеризующего башню")]
		private TowerProperty towerProperty;

		/// <summary>
        /// Текстовый компонент UI отображающий стоимость башни
        /// </summary>
		[SerializeField, Tooltip("Текстовый компонент UI отображающий стоимость башни")]
		private Text costText;

		/// <summary>
        /// UI кнопка для выбора башни
        /// </summary>
		[SerializeField, Tooltip("UI кнопка для выбора башни")]
		public Button button;

		/// <summary>
        /// Устанавливает отображаемую цену в магазине соответсвено свойству башни 
        /// </summary>
		public void SetCost() {
			if (costText != null && towerProperty != null)
				costText.text = towerProperty.Cost.ToString();
			else if (button != null)
				button.interactable = false;
		}

		/// <summary>
        /// Обновляет интерактивность кнопки выбора башени согласно имеющимся в распоряжении монетам 
        /// </summary>
        /// <param name="coins">Имеющиеся в распоряжении монеты</param>
		public void UpdateButtonsInteractable(int coins) {
			if (button == null)
				return;
			
			if (towerProperty != null)
				button.interactable = (coins >= towerProperty.Cost);
			else 
				button.interactable = false;				
		}

		/// <summary>
        /// Делает кнопку не активной
        /// </summary>
		public void DisableButton() {
			if (button)
				button.interactable = false;
		}

		/// <summary>
        /// Устанваливает кнопку в режим прослушивания, что бы в момент когда она нажимается создавалась башня-призрак
        /// </summary>
		public void SetButtonToListenCreateGhostTower() {
			if (button != null)
				button.onClick.AddListener(CreateGhostTower);
		}

		/// <summary>
        /// Добавляет listener к кнопке
        /// </summary>
		public void AddButtonListener(UnityAction action) {
			if (button != null)
				button.onClick.AddListener(action);
		}

		/// <summary>
        /// Создаёт башню-призрак
        /// </summary>
		private void CreateGhostTower() {
			if (towerGhostPref == null)
				return;

			button.interactable = false;
			BuildTower buildTower = UnityEngine.Object.Instantiate(towerGhostPref).GetComponent<BuildTower>();


			if (buildTower != null) {
				buildTower.AddBuildTowerCallback(this);
				buildTower.AddCancleBuildTowerCallback(this);
			}
		}

		/// <summary>
        /// Callback метод вызываемый при отмене строительства башни
        /// </summary>
		public void CanceledBuildTower() {
			if (unkCallbackEvent != null)
				unkCallbackEvent();
		}

		/// <summary>
        /// Callback метод вызываемый при удачном строительстве башни
        /// </summary>
		public void BuildedTower() {
			if (unkCallbackEvent != null)
				unkCallbackEvent();
		}
		
		/// <summary>
        /// Callback Event для события создания или отмены создания башни
        /// </summary>
		private event Action unkCallbackEvent;

		/// <summary>
        /// Добавляет callback для события создания или отмены создания башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(ISetNewTower i) {
            unkCallbackEvent += i.SetNewTower;			
        }

		/// <summary>
        /// Открепляет callback для события создания или отмены создания башни
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(ISetNewTower i) {
            unkCallbackEvent -= i.SetNewTower;
        }
		
	}
}