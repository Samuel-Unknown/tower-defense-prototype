﻿using UnityEngine;
using UnityEngine.AI;
using System;

namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания события гибели монстра
    /// </summary>
	public interface IMonsterDied {
		/// <summary>
        /// Callback метод вызываемый после смерти монстра 
        /// </summary>
		void MonsterDied();
	}

	/// <summary>
    /// Компонент монстра
    /// </summary>
	[SelectionBase, DisallowMultipleComponent, RequireComponent (typeof (BoxCollider)), RequireComponent (typeof (NavMeshAgent))]
	public class Monster : MonoBehaviour {

		/// <summary>
        /// Экземпляр класса MonsterProperty характеризующего монстра
        /// </summary>
		[SerializeField, Tooltip("Экземпляр класса MonsterProperty характеризующего монстра")]
		private MonsterProperty monsterProperty;

		/// <summary>
        /// Текущий уровень здоровья
        /// </summary>
		private float currentHealth;

		/// <summary>
        /// Callback Event для события смерти монстра
        /// </summary>
		private event Action diedCallbackEvent;

		/// <summary>
        /// Добавляет callback для события смерти монстра
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(IMonsterDied i) {
            diedCallbackEvent += i.MonsterDied;
        }

		/// <summary>
        /// Открепляет callback для события смерти монстра
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(IMonsterDied i) {
            diedCallbackEvent -= i.MonsterDied;
        }

		private NavMeshAgent agent;

		void Awake() {
			GetComponent<BoxCollider>().isTrigger = true;
			agent = GetComponent<NavMeshAgent>();
		}

		void OnEnable() {
			currentHealth = monsterProperty.Health;

			// Находим главное здание
			GameObject mainBuilding = GameObject.FindWithTag("Player");

			// Задаём цель к которой идёт монстр согласно NavMeshAgent'у
			if (mainBuilding != null)
	          	agent.destination = mainBuilding.transform.position;

			agent.speed = monsterProperty.Speed;
		}

		void OnDisable() {
			diedCallbackEvent = null;
		}

		/// <summary>
        /// Смерть монстра
        /// </summary>
		public void Die() {
			if (diedCallbackEvent != null)
	        	diedCallbackEvent();

			gameObject.SetActive(false);
		}

		/// <summary>
        /// Наносит повреждения монстру
        /// </summary>
        /// <param name="damage">Урон</param>		
		public virtual void TakeDamage(float damage) {
			currentHealth = Mathf.Clamp(currentHealth - damage, 0, monsterProperty.Health);
			if (currentHealth == 0) {
				Die();
				GameInfo.Instance.Coins += Reward;
			}
		}

		/// <summary>
        /// Возвращает урон наносимый от контакта
        /// </summary>
		public float ContactDamage {
			get { return monsterProperty.ContactDamage; }
		} 

		/// <summary>
        /// Возвращает награду за уничтожение монстра
        /// </summary>
		public int Reward {
			get { return monsterProperty.Reward; }
		}
	}
}