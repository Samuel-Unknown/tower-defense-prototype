﻿using UnityEngine;

namespace TDGame {

	/// <summary>
    /// Компонент башни
    /// </summary>
	[SelectionBase, DisallowMultipleComponent, RequireComponent (typeof (Rigidbody)), RequireComponent (typeof (BoxCollider))]
	public class Tower : MonoBehaviour {

		/// <summary>
        /// Экземпляр класса TowerProperty характеризующего башню
        /// </summary>
		[SerializeField, Tooltip("Экземпляр класса TowerProperty характеризующего башню")]
		private TowerProperty towerProperty;

		/// <summary>
        /// Рисовать ли в эдиторе радиус действия башни 
        /// </summary>
		[SerializeField, Tooltip("Рисовать ли в эдиторе радиус действия башни")]
		private bool showDetectionRadius = true;

		/// <summary>
        /// Маска для нахождения ближайшего врага 
        /// </summary>
		private int layerMask;

		/// <summary>
        /// Мировые координаты башни в сцене
        /// </summary>
		private Vector3 towerPosition;

		/// <summary>
        /// Массив орудий навешанных на башню
        /// </summary>
		private AbstractWeapon[] weapons;

		void Awake() {
			GetComponent<Rigidbody>().isKinematic = true;
			GetComponent<BoxCollider>().isTrigger = true;

			layerMask = LayerMask.GetMask("Enemy");
			weapons = GetComponentsInChildren<AbstractWeapon>();

			towerPosition = transform.position;
		}
		
		void FixedUpdate () {
			// Проверяем нет ли в зоне поражения какой-нибудь цели.
			
			Vector3 target; 									 // цель по которой будет открыт огонь
			bool found = FindClosestMonsterPosition(out target); // найдена ли цель

			// Пробегаемся по списку орудий навешанных на башню,
			// и устанавливаем для них цели..
			for (int i = 0; i < weapons.Length; i++)
				weapons[i].SetTarget(found, target);
		}

		void OnDrawGizmos() {
			// Рисуем в эдиторе радиус действия башни
			if (showDetectionRadius && towerProperty) {
	        	Gizmos.color = Color.yellow;
    	    	Gizmos.DrawWireSphere(transform.position, towerProperty.DetectionRadius);
			}
		}

		/// <summary>
        /// Поиск ближайшего монстра в зоне действия башни
        /// </summary>
        /// <param name="closestMonsterPosition">Мировые координаты ближайшего монстра, если такового нет, то в вектор будет равен Vector3.zero</param>
        /// <returns>true - если нашли монстра в зоне действия башни, false - если не нашли</returns>
		bool FindClosestMonsterPosition(out Vector3 closestMonsterPosition) {
			closestMonsterPosition = Vector3.zero;
			
			Collider[] hitColliders = Physics.OverlapSphere(towerPosition, towerProperty.DetectionRadius, layerMask);
			float distance = Mathf.Infinity;
			bool found = false;

			// поиск ближайшего из найденных монстров
			for (int i = 0; i < hitColliders.Length; i++) {
				Vector3 monsterPosition = hitColliders[i].transform.position;
				Vector3 diff = monsterPosition - towerPosition;
				
				float curDistance = diff.sqrMagnitude;
				if (curDistance < distance) {
					distance = curDistance;
					closestMonsterPosition = monsterPosition;
					found = true;
				}	
			}
			
			return found;
		}

		/// <summary>
        /// Возвращает стоимость башни в магазине
        /// </summary>
		public int Cost { 
			get { 
				if (towerProperty != null)
					return towerProperty.Cost; 
				
				return 0;
			}
		}

		/// <summary>
        /// Возвращает радиус обнаружения целей [м]
        /// </summary>
		public float DetectionRadius { 
			get { 
				if (towerProperty != null)
					return towerProperty.DetectionRadius; 
				
				return 0;
			}
		}

	}
}
