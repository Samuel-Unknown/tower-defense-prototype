﻿using UnityEngine;

namespace TDGame {

	/// <summary>
    /// Класс характеризующий монстра 
    /// </summary>
	[CreateAssetMenu(menuName = "TDGame/Monster Property")]
	public class MonsterProperty : ScriptableObject {
		
		/// <summary>
        /// Скорость передвижения
        /// </summary>
		[SerializeField, Range(1, 15), Tooltip("Скорость передвижения")]
		private float speed = 1;

		/// <summary>
        /// Возвращает скорость передвижения монстра
        /// </summary>
		public float Speed { 
			get { return speed; }
		}

		/// <summary>
        /// Урон наносимый от контакта
        /// </summary>
		[SerializeField, Range(0.1f, 50), Tooltip("Урон наносимый от контакта")]
		private float contactDamage = 10;

		/// <summary>
        /// Возвращает урон наносимый от контакта
        /// </summary>
		public float ContactDamage {
			get { return contactDamage; }
		}

		/// <summary>
        /// Начальный уровень здоровья
        /// </summary>
		[SerializeField, Range(1, 250), Tooltip("Начальный уровень здоровья")]
		private float health = 50;

		/// <summary>
        /// Возвращает начальный уровень здоровья
        /// </summary>
		public float Health {
			get { return health; }
		}

		/// <summary>
        /// Награда за уничтожение
        /// </summary>
		[SerializeField, Range(0, 250), Tooltip("Награда за уничтожение")]
		private int reward = 50;

		/// <summary>
        /// Возвращает награду за уничтожение монстра
        /// </summary>
		public int Reward {
			get { return reward; }
		}
		
		/// <summary>
        /// Описание монстра
        /// </summary>
		[SerializeField, Tooltip("Описание монстра")]
		private string description;

		/// <summary>
        /// Возвращает описание монстра 
        /// </summary>
		public string Description {
			get { return description; }
		}
	}
}
