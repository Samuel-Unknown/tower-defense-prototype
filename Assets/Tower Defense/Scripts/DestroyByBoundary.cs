﻿using UnityEngine;

namespace TDGame {

	/// <summary>
    /// Класс для деактивирования всех объектов вылетевших за границу тригера
    /// </summary>
	[DisallowMultipleComponent, RequireComponent (typeof (BoxCollider))]
	public class DestroyByBoundary : MonoBehaviour {

		void Awake() {
			GetComponent<BoxCollider>().isTrigger = true;
		}
		
		void OnTriggerExit(Collider other) {
			other.gameObject.SetActive(false);
		}
	}
}