﻿using UnityEngine;
using System.Collections.Generic;

namespace TDGame {
	
	/// <summary>
    /// Компонент оружия башни
    /// </summary>
	[SelectionBase, DisallowMultipleComponent]
	public class WeaponGun : AbstractWeapon {

		/// <summary>
        /// Префаб снаряда выпускаемого оружием 
        /// </summary>
		[SerializeField, Tooltip("Префаб снаряда выпускаемого оружием")]
		private GameObject projectilePrefab;

		/// <summary>
        /// Количество элементов пула
        /// </summary>
		[SerializeField, Range(1, 100), Tooltip("Количество элементов пула")]
		private int pooledAmount = 20;

		/// <summary>
        /// Пул снарядов
        /// </summary>
		private List<Bullet> pool;

		void Awake() {
			pool = new List<Bullet>();

			if (projectilePrefab == null) {
				return;
			}

			for (int i = 0; i < pooledAmount; i++) {
				GameObject projectileObject = Instantiate(projectilePrefab, transform.position, Quaternion.identity) as GameObject;
     			projectileObject.SetActive(false);
				
				Projectile projectile = projectileObject.GetComponent<Projectile>();

				pool.Add(new Bullet(projectileObject, projectile));
			}
		}

		/// <summary>
        /// Осуществляет выстрел по заданной цели путём создания снаряда
        /// </summary>
        /// <param name="target">Глобальные координаты цели</param>
		protected override void Shoot(Vector3 target) {
			
			for (int i = 0; i < pooledAmount; i++) {
				if (!pool[i].go.activeInHierarchy) {

					
					pool[i].go.transform.position = transform.position;
					pool[i].go.transform.rotation = transform.rotation;

					if (pool[i].projectile) {
						pool[i].projectile.TargetDirection = target - transform.position;
						pool[i].projectile.Damage = weaponProperty.Damage;
					}

     				pool[i].go.SetActive(true);
					
					return;
				}
			}

            // Если не хватает снарядов из пула, увеличиваем пул
			GameObject projectileObject = Instantiate(projectilePrefab, transform.position, Quaternion.identity) as GameObject;
     		projectileObject.SetActive(true);
			
			Projectile projectile = projectileObject.GetComponent<Projectile>();
			if (projectile) {
				projectile.TargetDirection = target - transform.position;
				projectile.Damage = weaponProperty.Damage;
			}

			pool.Add(new Bullet(projectileObject, projectile));
			pooledAmount = pool.Count;
		}

		/// <summary>
        /// Структура из описания снаряда
        /// </summary>
		[System.Serializable]
		public struct Bullet {

			public Bullet(GameObject go, Projectile projectile) {
				this.go = go;
				this.projectile = projectile;
			}

			/// <summary>
            /// GameObject снаряда
            /// </summary>
			public GameObject go;

			/// <summary>
            /// Projectile снаряда
            /// </summary>
			public Projectile projectile;
		}
	}
}
