﻿using UnityEngine;

namespace TDGame {
	
	/// <summary>
    /// Класс характеризующий оружие башни 
    /// </summary>
	[CreateAssetMenu(menuName = "TDGame/Weapon Property")]
	public class WeaponProperty : ScriptableObject {

		/// <summary>
        /// Урон наносимый оружием
        /// </summary>
		[SerializeField, Range(0.1f, 50), Tooltip("Урон наносимый оружием")]
		private float damage = 10;

		/// <summary>
        /// Возвращает урон наносимый оружием
        /// </summary>
		public float Damage {
			get { return damage; }
		}

		/// <summary>
        /// Частота выстрелов [сек]
        /// </summary>
		[SerializeField, Range(0, 10), Tooltip("Частота выстрелов [сек]")]
		private float rateOfFire = 2;

		/// <summary>
        /// Возвращает частоту выстрелов [сек]
        /// </summary>
		public float RateOfFire {
			get { return rateOfFire; }
		}
		
	}
}
