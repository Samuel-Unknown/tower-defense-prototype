﻿using UnityEngine;
using System;

namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания события изменения текущего количества монет
    /// </summary>
	public interface ICoinsUpdated {

		/// <summary>
        /// Callback метод вызываемый обновлении количества монет
        /// </summary>
		void CoinsUpdated(int coins);
	}

	/// <summary>
    /// Синглтон класс с информацие о текущем количестве монет
    /// </summary>
	public class GameInfo {

		/// <summary>
        /// Callback Event для события обновления количества монет
        /// </summary>
		private event Action<int> сoinsUpdatedCallbackEvent;

		/// <summary>
        /// Добавляет callback для события обновления количества монет
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(ICoinsUpdated i) {
            сoinsUpdatedCallbackEvent += i.CoinsUpdated;
        }

		/// <summary>
        /// Открепляет callback для события обновления количества монет
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(ICoinsUpdated i) {
            сoinsUpdatedCallbackEvent -= i.CoinsUpdated;
        }

		private GameInfo() { ResetCoins(); }	
		
		private static GameInfo instance = null;

		/// <summary>
        /// Возвращает экземпляр синглтон класса
        /// </summary>
		public static GameInfo Instance {
			get {
				if (instance == null) {
					instance = new GameInfo();
				}

				return instance;
			}
		}

		/// <summary>
        /// Текущее количество монет
        /// </summary>
		private int coins;

		/// <summary>
        /// Текущее количество монет
        /// </summary>
		public int Coins {
			get { return coins; }

			set {
				coins = Mathf.Clamp(value, 0, int.MaxValue);
				
				if (сoinsUpdatedCallbackEvent != null)	
					сoinsUpdatedCallbackEvent(coins);
			}
		}

		/// <summary>
        /// Сбрасывает число монет в исходное состояние 
        /// </summary>
		public void ResetCoins() {
			Coins = 300;
		}
	}
}