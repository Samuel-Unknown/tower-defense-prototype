﻿using UnityEngine;
using UnityEngine.UI;

using System;


namespace TDGame {

	/// <summary>
    /// Интерфейс для отлавливания события начала игры
    /// </summary>
	public interface IStartGame {

		/// <summary>
        /// Callback метод вызываемый при запуске игры
        /// </summary>
		void StartGame();
	}

	/// <summary>
    /// Компонент отвечающий за логику работы с GUI
    /// </summary>
	[SelectionBase, DisallowMultipleComponent]
	public sealed class GUILogic : MonoBehaviour, IGameower, IAllWavesEnded, 
								   IMainBuildingHealthChanged, IWavesInfo, ICoinsUpdated {

		/// <summary>
		/// Текстовый компонент UI отображающий текущее количество монет игрока
		/// </summary>
		[SerializeField, Tooltip("Текстовый компонент UI отображающий текущее количество монет игрока")]
		private Text currentCoins;

		/// <summary>
		/// Текстовый компонент UI отображающий текущий уровень жизней игрока
		/// </summary>
		[SerializeField, Tooltip("Текстовый компонент UI отображающий текущий уровень жизней игрока")]
		private Text currentHealth;

		/// <summary>
		/// Текстовый компонент UI отображающий информацию о том сколько волн пройдено и из скольки
		/// </summary>
		[SerializeField, Tooltip("Текстовый компонент UI отображающий информацию о том сколько волн пройдено и из скольки")]
		private Text wavesInfo;

		/// <summary>
		/// GameObject с кнопкой для запуска игры 
		/// </summary>
		[SerializeField, Tooltip("GameObject с кнопкой для запуска игры")]	
		private GameObject buttonStartGO;

		/// <summary>
		/// GameObject магазина с башнями для покупок
		/// </summary>
		[SerializeField, Tooltip("GameObject магазина с башнями для покупок")]	
		private GameObject shopGO;

		/// <summary>
		/// GameObject с текстом "Вы выиграли"
		/// </summary>
		[SerializeField, Tooltip("GameObject с текстом \"Вы выиграли\"")]
		private GameObject youWinGO;

		/// <summary>
		/// GameObject с текстом "Вы проиграли"
		/// </summary>
		[SerializeField, Tooltip("GameObject с текстом \"Вы проиграли\"")]
		private GameObject youLoseGO;

		/// <summary>
        /// Callback Event для события начала игры
        /// </summary>
		private event Action startGameCallbackEvent;

		/// <summary>
        /// Добавляет callback для события начала игры
        /// </summary>
        /// <param name="i">Интерфейс</param>
		public void AddCallback(IStartGame i) {
            startGameCallbackEvent += i.StartGame;
        }

		/// <summary>
        /// Открепляет callback для события начала игры
        /// </summary>
        /// <param name="i">Интерфейс</param>
        public void RemoveCallback(IStartGame i) {
            startGameCallbackEvent -= i.StartGame;
        }

		MainBuilding mainBuilding;

		void Awake() {

			MonstersFactory monstersFactory = GameObject.FindObjectOfType<MonstersFactory>();
			if (monstersFactory) {
				monstersFactory.AddCallbackWavesInfo((IWavesInfo)this);
				monstersFactory.AddCallbackWavesEnded((IAllWavesEnded)this);
			}

			GameInfo.Instance.AddCallback(this);
			SetСurrentCoins(GameInfo.Instance.Coins);

			Button button = buttonStartGO.GetComponent<Button>();
			if (button != null)
				button.onClick.AddListener(StartGame);
		}

		void Update() {
        	if (Input.GetKey(KeyCode.Escape))
        	    Application.Quit();
    	}

		/// <summary>
		/// Устанавливает отображаемое число монет доступных игроку
		/// </summary>
		/// <param name="coins">Число монет</param>
		private void SetСurrentCoins(int coins) {
			if (currentCoins)
				currentCoins.text = coins.ToString();
		}

		/// <summary>
		/// Устанавливает отображаемый уровень жизней игрока
		/// </summary>
		/// <param name="health">Уровень жизней игрока</param>
		private void SetСurrentHealth(float health) {
			if (currentHealth)
				currentHealth.text = health.ToString("0");
		}

		/// <summary>
		/// Устанавливает отображаемую информацию о том сколько волн пройдено и из скольки
		/// </summary>
		/// <param name="currentWave">Номер текущей волны</param>
		/// <param name="maxWave">Максимальное число волн</param>
		private void SetWavesInfo(int currentWave, int maxWave) {
			if (wavesInfo)
				wavesInfo.text = currentWave.ToString() + "/" + maxWave.ToString();
		}

		/// <summary>
        /// Callback метод для получения информации о номере текущей волны и их общем количестве 
        /// </summary>
        /// <param name="currentWave">номер текущей волны</param>
        /// <param name="maxWave">максимальное число волн</param>
		public void WavesInfo(int currentWave, int maxWave) {
			SetWavesInfo(currentWave, maxWave);
		}

		/// <summary>
        /// Callback метод вызываемый при ранении главного здания
        /// </summary>
		public void MainBuildingHealthChanged(float currentHealth) {
			SetСurrentHealth(currentHealth);
		}

		/// <summary>
        /// Запуск игры
        /// </summary>
		public void StartGame() {
			if (buttonStartGO != null)
				buttonStartGO.SetActive(false);

			if (shopGO != null)
				shopGO.SetActive(false);

			if (youWinGO != null)
				youWinGO.SetActive(false);
		
			if (youLoseGO != null)
				youLoseGO.SetActive(false);

			if (startGameCallbackEvent != null)
				startGameCallbackEvent();

			ListeMainBuilding();

			if (mainBuilding)
				SetСurrentHealth(mainBuilding.CurrentHealth);
		}

		/// <summary>
        /// Подписывается на события главного здания 
        /// </summary>
		void ListeMainBuilding() {
			mainBuilding = GameObject.FindObjectOfType<MainBuilding>();
			
			if (mainBuilding) {
				mainBuilding.AddCallback((IGameower)this);			
				mainBuilding.AddCallback((IMainBuildingHealthChanged)this);

				SetСurrentHealth(mainBuilding.CurrentHealth);
			}
		}

		/// <summary>
        /// Завершение игры
        /// </summary>
		public void EndGame(bool win) {
			if (buttonStartGO != null)
				buttonStartGO.SetActive(true);

			if (shopGO != null)
				shopGO.SetActive(true);

			if (youWinGO != null)
				youWinGO.SetActive(win);
		
			if (youLoseGO != null)
				youLoseGO.SetActive(!win);
		}

		/// <summary>
        /// Callback метод вызываемый при гибели гавного здания
        /// </summary>
		public void Gameower() {
			EndGame(false);
		}

		/// <summary>
        /// Callback метод вызываемый при завершении всех волн
        /// </summary>
		public void AllWavesEnded() {
			EndGame(true);
		}

		/// <summary>
        /// Callback метод вызываемый при обновлении количества монет
        /// </summary>
		public void CoinsUpdated(int coins) {
			SetСurrentCoins(coins);
		}
	}
}
